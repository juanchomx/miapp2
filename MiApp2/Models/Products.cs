﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace MiApp2.Models
{
    [Table("Product")]
    public class Products
    {
        [Key]
        [Display(Name = "ProductID")]
        public int ProductID { get; set; }
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Display(Name = "ProductNumber")]
        public string ProductNumber { get; set; }
        [Display(Name = "Color")]
        public string Color { get; set; }
        [Display(Name = "StandardCost")]
        public decimal StandardCost { get; set; }
        [Display(Name = "ListPrice")]
        public decimal ListPrice { get; set; }
        [Display(Name = "rowguid")]
        public string rowguid { get; set; }

    }
}