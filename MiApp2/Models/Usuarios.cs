﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace MiApp2.Models
{
    [Table("Usuarios")]
    public class Usuarios
    {
        [Key]
        [Display(Name = "UsuarioID")]
        public int UsuarioID { get; set; }

        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Display(Name = "Matricula")]
        public string Matricula { get; set; }

        [Display(Name = "Curso")]
        public string Curso { get; set; }

        [Display(Name = "CursoAprobado")]
        public bool CursoAprobado { get; set; }
    }
}