﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MiApp2.Controllers
{
    public class HomeController : Controller
    {
        #region
        public const string azure_datasource = "<server>.database.windows.net";
        public const string azure_database = "<db>";
        public const string azure_username = "<user>"; //username of server to connect
        public const string azure_password = "<password>"; //password   
        #endregion

        public const string connString = @"Data Source=" + azure_datasource + ";Initial Catalog="
                        + azure_database + ";Persist Security Info=True;User ID=" + azure_username + ";Password=" + azure_password;

        public ActionResult Paginicio()
        {
                string strQry = "SELECT * FROM SalesLT.Product";

                DataTable dt = new DataTable();

                SqlConnection _conn = new SqlConnection(connString);

                using (_conn)
                {
                    _conn.Open();

                    using (SqlCommand command = new SqlCommand(strQry, _conn))
                    {
                        command.Connection = _conn;
                        using (SqlDataAdapter sda = new SqlDataAdapter(command))
                        {
                            sda.Fill(dt);
                        }
                    }
                }          
            

            ViewBag.mensaje = "Estas en Paginicio";

            return View(dt);
        }

        public ActionResult DetallesProducto (int? prodID)
        {
            string strQry = "SELECT * FROM SalesLT.Product WHERE ProductID = " + prodID;


            DataTable dt = new DataTable();

            SqlConnection _conn = new SqlConnection(connString);

            using (_conn)
            {
                _conn.Open();

                using (SqlCommand command = new SqlCommand(strQry, _conn))
                {
                    command.Connection = _conn;
                    using (SqlDataAdapter sda = new SqlDataAdapter(command))
                    {
                        sda.Fill(dt);
                    }
                }
            }


            ViewBag.mensaje = "Detalle del Producto";

            return View(dt);
        }

        public ActionResult EnConstruccion()
        {
            return View();
        }

        public ActionResult Formulario1()
        {

            
            return View();
        }

        public ActionResult RevisaForm(int? tipoUsuario, string nombre)
        {
            //int tipoUsuario = 0;//Init

            if (tipoUsuario == 1)
            {
                ViewBag.TipodeUsuario = "Profesor";
                return View("PaginaDos");
            }
            else
            {
                ViewBag.TipodeUsuario = "Alumno";

                //return Redirect("EnConstruccion");
                return RedirectToAction("Paginicio");
            }
        }

    }
}